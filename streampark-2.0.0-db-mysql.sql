-- streamPark 2.0.0版本 mysql数据库脚本
-- https://github.com/apache/incubator-streampark/tree/dev/streampark-console/streampark-console-service/src/main/resources/db

-- streampark.t_access_token definition

CREATE TABLE `t_access_token` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'key',
  `user_id` bigint(20) DEFAULT NULL,
  `token` varchar(1024) DEFAULT NULL COMMENT 'token',
  `expire_time` datetime DEFAULT NULL COMMENT 'expiration',
  `description` varchar(512) DEFAULT NULL COMMENT 'description',
  `status` tinyint(4) DEFAULT NULL COMMENT '1:enable,0:disable',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'create time',
  `modify_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'modify time',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;


-- streampark.t_alert_config definition

CREATE TABLE `t_alert_config` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) DEFAULT NULL,
  `alert_name` varchar(128) DEFAULT NULL COMMENT 'alert group name',
  `alert_type` int(11) DEFAULT '0' COMMENT 'alert type',
  `email_params` varchar(255) DEFAULT NULL COMMENT 'email params',
  `sms_params` text COMMENT 'sms params',
  `ding_talk_params` text COMMENT 'ding talk params',
  `we_com_params` varchar(255) DEFAULT NULL COMMENT 'wechat params',
  `http_callback_params` text COMMENT 'http callback params',
  `lark_params` text COMMENT 'lark params',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'create time',
  `modify_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'change time',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;


-- streampark.t_app_backup definition

CREATE TABLE `t_app_backup` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `app_id` bigint(20) DEFAULT NULL,
  `sql_id` bigint(20) DEFAULT NULL,
  `config_id` bigint(20) DEFAULT NULL,
  `version` int(11) DEFAULT NULL,
  `path` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'create time',
  `modify_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'modify time',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;


-- streampark.t_app_build_pipe definition

CREATE TABLE `t_app_build_pipe` (
  `app_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `pipe_type` tinyint(4) DEFAULT NULL,
  `pipe_status` tinyint(4) DEFAULT NULL,
  `cur_step` smallint(6) DEFAULT NULL,
  `total_step` smallint(6) DEFAULT NULL,
  `steps_status` text,
  `steps_status_ts` text,
  `error` text,
  `build_result` text,
  `modify_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'modify time',
  PRIMARY KEY (`app_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;


-- streampark.t_flame_graph definition

CREATE TABLE `t_flame_graph` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `app_id` bigint(20) DEFAULT NULL,
  `profiler` varchar(255) DEFAULT NULL,
  `timeline` datetime DEFAULT NULL,
  `content` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;


-- streampark.t_flink_app definition

CREATE TABLE `t_flink_app` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `team_id` bigint(20) NOT NULL,
  `job_type` tinyint(4) DEFAULT NULL,
  `execution_mode` tinyint(4) DEFAULT NULL,
  `resource_from` tinyint(4) DEFAULT NULL,
  `project_id` varchar(64) DEFAULT NULL,
  `job_name` varchar(255) DEFAULT NULL,
  `module` varchar(255) DEFAULT NULL,
  `jar` varchar(255) DEFAULT NULL,
  `jar_check_sum` bigint(20) DEFAULT NULL,
  `main_class` varchar(255) DEFAULT NULL,
  `args` text,
  `options` text,
  `hot_params` text,
  `user_id` bigint(20) DEFAULT NULL,
  `app_id` varchar(255) DEFAULT NULL,
  `app_type` tinyint(4) DEFAULT NULL,
  `duration` bigint(20) DEFAULT NULL,
  `job_id` varchar(64) DEFAULT NULL,
  `job_manager_url` varchar(255) DEFAULT NULL,
  `version_id` bigint(20) DEFAULT NULL,
  `cluster_id` varchar(255) DEFAULT NULL,
  `k8s_namespace` varchar(255) DEFAULT NULL,
  `flink_image` varchar(255) DEFAULT NULL,
  `state` varchar(50) DEFAULT NULL,
  `restart_size` int(11) DEFAULT NULL,
  `restart_count` int(11) DEFAULT NULL,
  `cp_threshold` int(11) DEFAULT NULL,
  `cp_max_failure_interval` int(11) DEFAULT NULL,
  `cp_failure_rate_interval` int(11) DEFAULT NULL,
  `cp_failure_action` tinyint(4) DEFAULT NULL,
  `dynamic_properties` text,
  `description` varchar(255) DEFAULT NULL,
  `resolve_order` tinyint(4) DEFAULT NULL,
  `k8s_rest_exposed_type` tinyint(4) DEFAULT NULL,
  `flame_graph` tinyint(4) DEFAULT '0',
  `jm_memory` int(11) DEFAULT NULL,
  `tm_memory` int(11) DEFAULT NULL,
  `total_task` int(11) DEFAULT NULL,
  `total_tm` int(11) DEFAULT NULL,
  `total_slot` int(11) DEFAULT NULL,
  `available_slot` int(11) DEFAULT NULL,
  `option_state` tinyint(4) DEFAULT NULL,
  `tracking` tinyint(4) DEFAULT NULL,
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'create time',
  `modify_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'modify time',
  `option_time` datetime DEFAULT NULL,
  `launch` tinyint(4) DEFAULT '1',
  `build` tinyint(4) DEFAULT '1',
  `start_time` datetime DEFAULT NULL,
  `end_time` datetime DEFAULT NULL,
  `alert_id` bigint(20) DEFAULT NULL,
  `k8s_pod_template` text,
  `k8s_jm_pod_template` text,
  `k8s_tm_pod_template` text,
  `k8s_hadoop_integration` tinyint(4) DEFAULT '0',
  `flink_cluster_id` bigint(20) DEFAULT NULL,
  `ingress_template` text,
  `default_mode_ingress` text,
  `tags` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=100001 DEFAULT CHARSET=utf8mb4;


-- streampark.t_flink_cluster definition

CREATE TABLE `t_flink_cluster` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `address` varchar(255) DEFAULT NULL COMMENT 'url address of jobmanager',
  `cluster_id` varchar(255) DEFAULT NULL COMMENT 'clusterid of session mode(yarn-session:application-id,k8s-session:cluster-id)',
  `cluster_name` varchar(255) NOT NULL COMMENT 'cluster name',
  `options` text COMMENT 'json form of parameter collection ',
  `yarn_queue` varchar(100) DEFAULT NULL COMMENT 'the yarn queue where the task is located',
  `execution_mode` tinyint(4) NOT NULL DEFAULT '1' COMMENT 'k8s execution session mode(1:remote,3:yarn-session,5:kubernetes-session)',
  `version_id` bigint(20) NOT NULL COMMENT 'flink version id',
  `k8s_namespace` varchar(255) DEFAULT 'default' COMMENT 'k8s namespace',
  `service_account` varchar(50) DEFAULT NULL COMMENT 'k8s service account',
  `description` varchar(255) DEFAULT NULL,
  `user_id` bigint(20) DEFAULT NULL,
  `flink_image` varchar(255) DEFAULT NULL COMMENT 'flink image',
  `dynamic_properties` text COMMENT 'allows specifying multiple generic configuration options',
  `k8s_rest_exposed_type` tinyint(4) DEFAULT '2' COMMENT 'k8s export(0:loadbalancer,1:clusterip,2:nodeport)',
  `k8s_hadoop_integration` tinyint(4) DEFAULT '0',
  `flame_graph` tinyint(4) DEFAULT '0' COMMENT 'flameGraph enable，default disable',
  `k8s_conf` varchar(255) DEFAULT NULL COMMENT 'the path where the k 8 s configuration file is located',
  `resolve_order` tinyint(4) DEFAULT NULL,
  `exception` text COMMENT 'exception information',
  `cluster_state` tinyint(4) DEFAULT '0' COMMENT 'cluster status (0: created but not started, 1: started, 2: stopped)',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'create time',
  PRIMARY KEY (`id`,`cluster_name`),
  UNIQUE KEY `cluster_id` (`cluster_id`,`address`,`execution_mode`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;


-- streampark.t_flink_config definition

CREATE TABLE `t_flink_config` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `app_id` bigint(20) NOT NULL,
  `format` tinyint(4) NOT NULL DEFAULT '0',
  `version` int(11) NOT NULL,
  `latest` tinyint(4) NOT NULL DEFAULT '0',
  `content` text NOT NULL,
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'create time',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;


-- streampark.t_flink_effective definition

CREATE TABLE `t_flink_effective` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `app_id` bigint(20) NOT NULL,
  `target_type` tinyint(4) NOT NULL COMMENT '1) config 2) flink sql',
  `target_id` bigint(20) NOT NULL COMMENT 'configId or sqlId',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'create time',
  PRIMARY KEY (`id`),
  UNIQUE KEY `app_id` (`app_id`,`target_type`)
) ENGINE=InnoDB AUTO_INCREMENT=100001 DEFAULT CHARSET=utf8mb4;


-- streampark.t_flink_env definition

CREATE TABLE `t_flink_env` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `flink_name` varchar(128) NOT NULL COMMENT 'flink instance name',
  `flink_home` varchar(255) NOT NULL COMMENT 'flink home path',
  `version` varchar(50) NOT NULL COMMENT 'flink version',
  `scala_version` varchar(50) NOT NULL COMMENT 'scala version of flink',
  `flink_conf` text NOT NULL COMMENT 'flink-conf',
  `is_default` tinyint(4) NOT NULL DEFAULT '0' COMMENT 'whether default version or not',
  `description` varchar(255) DEFAULT NULL COMMENT 'description',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'create time',
  PRIMARY KEY (`id`),
  UNIQUE KEY `flink_name` (`flink_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;


-- streampark.t_flink_log definition

CREATE TABLE `t_flink_log` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `app_id` bigint(20) DEFAULT NULL,
  `yarn_app_id` varchar(50) DEFAULT NULL,
  `job_manager_url` varchar(255) DEFAULT NULL,
  `success` tinyint(4) DEFAULT NULL,
  `exception` text,
  `option_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;


-- streampark.t_flink_project definition

CREATE TABLE `t_flink_project` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `team_id` bigint(20) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `url` varchar(1000) DEFAULT NULL,
  `branches` varchar(1000) DEFAULT NULL,
  `user_name` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `pom` varchar(255) DEFAULT NULL,
  `build_args` varchar(255) DEFAULT NULL,
  `type` tinyint(4) DEFAULT NULL,
  `repository` tinyint(4) DEFAULT NULL,
  `last_build` datetime DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `build_state` tinyint(4) DEFAULT '-1',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'create time',
  `modify_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'modify time',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=100001 DEFAULT CHARSET=utf8mb4;


-- streampark.t_flink_savepoint definition

CREATE TABLE `t_flink_savepoint` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `app_id` bigint(20) NOT NULL,
  `chk_id` bigint(20) DEFAULT NULL,
  `type` tinyint(4) DEFAULT NULL,
  `path` varchar(255) DEFAULT NULL,
  `latest` tinyint(4) NOT NULL DEFAULT '1',
  `trigger_time` datetime DEFAULT NULL,
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'create time',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;


-- streampark.t_flink_sql definition

CREATE TABLE `t_flink_sql` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `app_id` bigint(20) DEFAULT NULL,
  `sql` text,
  `dependency` text,
  `version` int(11) DEFAULT NULL,
  `candidate` tinyint(4) NOT NULL DEFAULT '1',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'create time',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=100001 DEFAULT CHARSET=utf8mb4;


-- streampark.t_flink_tutorial definition

CREATE TABLE `t_flink_tutorial` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` tinyint(4) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `content` text,
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'create time',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;


-- streampark.t_member definition

CREATE TABLE `t_member` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `team_id` bigint(20) NOT NULL COMMENT 'team id',
  `user_id` bigint(20) NOT NULL COMMENT 'user id',
  `role_id` bigint(20) NOT NULL COMMENT 'role id',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'create time',
  `modify_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'modify time',
  PRIMARY KEY (`id`),
  UNIQUE KEY `team_id` (`team_id`,`user_id`,`role_id`)
) ENGINE=InnoDB AUTO_INCREMENT=100005 DEFAULT CHARSET=utf8mb4;


-- streampark.t_menu definition

CREATE TABLE `t_menu` (
  `menu_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'menu button id',
  `parent_id` bigint(20) NOT NULL COMMENT 'parent menu id',
  `menu_name` varchar(50) NOT NULL COMMENT 'menu button name',
  `path` varchar(255) DEFAULT NULL COMMENT 'routing path',
  `component` varchar(255) DEFAULT NULL COMMENT 'routing component component',
  `perms` varchar(50) DEFAULT NULL COMMENT 'authority id',
  `icon` varchar(50) DEFAULT NULL COMMENT 'icon',
  `type` char(2) DEFAULT NULL COMMENT 'type 0:menu 1:button',
  `display` tinyint(4) NOT NULL DEFAULT '0' COMMENT 'whether the menu is displayed',
  `order_num` int(11) DEFAULT NULL COMMENT 'sort',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'create time',
  `modify_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'modify time',
  PRIMARY KEY (`menu_id`)
) ENGINE=InnoDB AUTO_INCREMENT=100071 DEFAULT CHARSET=utf8mb4;


-- streampark.t_message definition

CREATE TABLE `t_message` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `app_id` bigint(20) DEFAULT NULL,
  `user_id` bigint(20) DEFAULT NULL,
  `type` tinyint(4) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `context` text,
  `is_read` tinyint(4) DEFAULT '0',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'create time',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;


-- streampark.t_role definition

CREATE TABLE `t_role` (
  `role_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'user id',
  `role_name` varchar(50) NOT NULL COMMENT 'user name',
  `remark` varchar(100) DEFAULT NULL COMMENT 'remark',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'create time',
  `modify_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'modify time',
  `role_code` varchar(255) DEFAULT NULL COMMENT 'role code',
  PRIMARY KEY (`role_id`)
) ENGINE=InnoDB AUTO_INCREMENT=100003 DEFAULT CHARSET=utf8mb4;


-- streampark.t_role_menu definition

CREATE TABLE `t_role_menu` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `role_id` bigint(20) NOT NULL,
  `menu_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `role_id` (`role_id`,`menu_id`)
) ENGINE=InnoDB AUTO_INCREMENT=100099 DEFAULT CHARSET=utf8mb4;


-- streampark.t_setting definition

CREATE TABLE `t_setting` (
  `order_num` int(11) DEFAULT NULL,
  `setting_key` varchar(50) NOT NULL,
  `setting_value` text,
  `setting_name` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `type` tinyint(4) NOT NULL COMMENT '1: input 2: boolean 3: number',
  PRIMARY KEY (`setting_key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;


-- streampark.t_team definition

CREATE TABLE `t_team` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'team id',
  `team_name` varchar(50) NOT NULL COMMENT 'team name',
  `description` varchar(255) DEFAULT NULL,
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'create time',
  `modify_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'modify time',
  PRIMARY KEY (`id`),
  UNIQUE KEY `team_name` (`team_name`)
) ENGINE=InnoDB AUTO_INCREMENT=100002 DEFAULT CHARSET=utf8mb4;


-- streampark.t_user definition

CREATE TABLE `t_user` (
  `user_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'user id',
  `username` varchar(255) NOT NULL COMMENT 'user name',
  `nick_name` varchar(50) NOT NULL COMMENT 'nick name',
  `salt` varchar(255) DEFAULT NULL COMMENT 'salt',
  `password` varchar(128) NOT NULL COMMENT 'password',
  `email` varchar(128) DEFAULT NULL COMMENT 'email',
  `user_type` int(11) NOT NULL COMMENT 'user type 1:admin 2:user',
  `last_team_id` bigint(20) DEFAULT NULL COMMENT 'last team id',
  `status` char(1) NOT NULL COMMENT 'status 0:locked 1:active',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'create time',
  `modify_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'modify time',
  `last_login_time` datetime DEFAULT NULL COMMENT 'last login time',
  `sex` char(1) DEFAULT NULL COMMENT 'gender 0:male 1:female 2:confidential',
  `avatar` varchar(100) DEFAULT NULL COMMENT 'avatar',
  `description` varchar(100) DEFAULT NULL COMMENT 'description',
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=100004 DEFAULT CHARSET=utf8mb4;


-- streampark.t_variable definition

CREATE TABLE `t_variable` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'variable id',
  `variable_code` varchar(100) NOT NULL COMMENT 'Variable code is used for parameter names passed to the program or as placeholders',
  `variable_value` text NOT NULL COMMENT 'The specific value corresponding to the variable',
  `description` text COMMENT 'More detailed description of variables, only for display, not involved in program logic',
  `creator_id` bigint(20) NOT NULL COMMENT 'user id of creator',
  `team_id` bigint(20) NOT NULL COMMENT 'team id',
  `desensitization` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0 is no desensitization, 1 is desensitization, if set to desensitization, it will be replaced by * when displayed',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'create time',
  `modify_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'modify time',
  PRIMARY KEY (`id`),
  UNIQUE KEY `team_id` (`team_id`,`variable_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;


-- 初始化数据

INSERT INTO t_access_token (user_id, token, expire_time, description, status, create_time, modify_time) VALUES(0, '', '', '', 0, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_alert_config (user_id, alert_name, alert_type, email_params, sms_params, ding_talk_params, we_com_params, http_callback_params, lark_params, create_time, modify_time) VALUES(0, '', 0, '', '', '', '', '', '', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_app_backup (app_id, sql_id, config_id, version, `path`, description, create_time, modify_time) VALUES(0, 0, 0, 0, '', '', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_app_build_pipe (pipe_type, pipe_status, cur_step, total_step, steps_status, steps_status_ts, error, build_result, modify_time) VALUES(0, 0, 0, 0, '', '', '', '', CURRENT_TIMESTAMP);
INSERT INTO t_flame_graph (app_id, profiler, timeline, content) VALUES(0, '', '', '');
INSERT INTO t_flink_app (team_id, job_type, execution_mode, resource_from, project_id, job_name, module, jar, jar_check_sum, main_class, args, `options`, hot_params, user_id, app_id, app_type, duration, job_id, job_manager_url, version_id, cluster_id, k8s_namespace, flink_image, state, restart_size, restart_count, cp_threshold, cp_max_failure_interval, cp_failure_rate_interval, cp_failure_action, dynamic_properties, description, resolve_order, k8s_rest_exposed_type, flame_graph, jm_memory, tm_memory, total_task, total_tm, total_slot, available_slot, option_state, tracking, create_time, modify_time, option_time, launch, build, start_time, end_time, alert_id, k8s_pod_template, k8s_jm_pod_template, k8s_tm_pod_template, k8s_hadoop_integration, flink_cluster_id, ingress_template, default_mode_ingress, tags) VALUES(0, 0, 0, 0, '', '', '', '', 0, '', '', '', '', 0, '', 0, 0, '', '', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '', 1, 1, '', '', 0, '', '', '', 0, 0, '', '', '');
INSERT INTO t_flink_cluster (address, cluster_id, cluster_name, `options`, yarn_queue, execution_mode, version_id, k8s_namespace, service_account, description, user_id, flink_image, dynamic_properties, k8s_rest_exposed_type, k8s_hadoop_integration, flame_graph, k8s_conf, resolve_order, `exception`, cluster_state, create_time) VALUES('', '', '', '', '', 1, 0, 'default', '', '', 0, '', '', 2, 0, 0, '', 0, '', 0, CURRENT_TIMESTAMP);
INSERT INTO t_flink_config (app_id, format, version, latest, content, create_time) VALUES(0, 0, 0, 0, '', CURRENT_TIMESTAMP);
INSERT INTO t_flink_effective (app_id, target_type, target_id, create_time) VALUES(0, 0, 0, CURRENT_TIMESTAMP);
INSERT INTO t_flink_env (flink_name, flink_home, version, scala_version, flink_conf, is_default, description, create_time) VALUES('', '', '', '', '', 0, '', CURRENT_TIMESTAMP);
INSERT INTO t_flink_log (app_id, yarn_app_id, job_manager_url, success, `exception`, option_time) VALUES(0, '', '', 0, '', '');
INSERT INTO t_flink_project (team_id, name, url, branches, user_name, password, pom, build_args, `type`, repository, last_build, description, build_state, create_time, modify_time) VALUES(0, '', '', '', '', '', '', '', 0, 0, '', '', -1, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_flink_savepoint (app_id, chk_id, `type`, `path`, latest, trigger_time, create_time) VALUES(0, 0, 0, '', 1, '', CURRENT_TIMESTAMP);
INSERT INTO t_flink_sql (app_id, `sql`, dependency, version, candidate, create_time) VALUES(0, '', '', 0, 1, CURRENT_TIMESTAMP);
INSERT INTO t_flink_tutorial (`type`, name, content, create_time) VALUES(0, '', '', CURRENT_TIMESTAMP);
INSERT INTO t_member (team_id, user_id, role_id, create_time, modify_time) VALUES(0, 0, 0, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_menu (parent_id, menu_name, `path`, component, perms, icon, `type`, display, order_num, create_time, modify_time) VALUES(0, '', '', '', '', '', '', 0, 0, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_message (app_id, user_id, `type`, title, context, is_read, create_time) VALUES(0, 0, 0, '', '', 0, CURRENT_TIMESTAMP);
INSERT INTO t_role (role_name, remark, create_time, modify_time, role_code) VALUES('', '', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '');
INSERT INTO t_role_menu (role_id, menu_id) VALUES(0, 0);
INSERT INTO t_setting (order_num, setting_key, setting_value, setting_name, description, `type`) VALUES(0, '', '', '', '', 0);
INSERT INTO t_team (team_name, description, create_time, modify_time) VALUES('', '', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
INSERT INTO t_user (username, nick_name, salt, password, email, user_type, last_team_id, status, create_time, modify_time, last_login_time, sex, avatar, description) VALUES('', '', '', '', '', 0, 0, '', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '', '', '', '');
INSERT INTO t_variable (variable_code, variable_value, description, creator_id, team_id, desensitization, create_time, modify_time) VALUES('', '', '', 0, 0, 0, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
